#ifndef HTTP_H
#define HTTP_H

int http_handle(const char* root, int client_fd);

#endif  // HTTP_H